package co.edu.arrobamedellin.arrobamedellin.Detallescarreras;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import co.edu.arrobamedellin.arrobamedellin.R;

public class DetallescarreraUnacional extends AppCompatActivity {

    Button btnirmaps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detallescarrera_unacional);


        btnirmaps = (Button) findViewById(R.id.btn_maps);
        btnirmaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cordenadas();

            }
        });

        String nombre_institucion = getIntent().getExtras().getString("nombre_institucion");
        String estado_institucion = getIntent().getExtras().getString("estado_institucion");
        String caracter_academico = getIntent().getExtras().getString("caracter_academico");
        String sector = getIntent().getExtras().getString("sector");
        String codigo_snies = getIntent().getExtras().getString("codigo_snies");
        String area_de_conococimiento = getIntent().getExtras().getString("area_de_conococimiento");
        String basico_de_conocimiento = getIntent().getExtras().getString("basico_de_conocimiento");
        String nombre_del_programa = getIntent().getExtras().getString("nombre_del_programa");
        String nivel_academico = getIntent().getExtras().getString("nivel_academico");
        String nivel_formacion = getIntent().getExtras().getString("nivel_formacion");
        String metodologia = getIntent().getExtras().getString("metodologia");
        String numero_periodos_de_duracion = getIntent().getExtras().getString("numero_periodos_de_duracion");
        String periodos_de_duracion = getIntent().getExtras().getString("periodos_de_duracion");
        String titulo_otorgado = getIntent().getExtras().getString("titulo_otorgado");
        String departamento_oferta_programa = getIntent().getExtras().getString("departamento_oferta_programa");
        String municipio_oferta_programa = getIntent().getExtras().getString("municipio_oferta_programa");
        String costo_matricula = getIntent().getExtras().getString("costo_matricula");
        String tiempo_admisiones_estudiantes = getIntent().getExtras().getString("tiempo_admisiones_estudiantes");
        String logo_universidad = getIntent().getExtras().getString("Logo universidad");



        TextView tv_nombre_institucion = findViewById(R.id.textView_111);
        TextView tv_caracter_academico = findViewById(R.id.textView_333);
        TextView tv_codigo_snies = findViewById(R.id.textView_555);
        TextView tv_area_de_conococimiento = findViewById(R.id.textView_666);
        TextView tv_basico_de_conocimiento = findViewById(R.id.textView_777);
        TextView tv_nombre_del_programa = findViewById(R.id.textView_888);
        TextView tv_nivel_academico = findViewById(R.id.textView_999);
        TextView tv_nivel_formacion = findViewById(R.id.textView_1000);
        TextView tv_metodologia = findViewById(R.id.textView_1111);
        TextView tv_numero_periodos_de_duracion = findViewById(R.id.textView_2222);
        TextView tv_periodos_de_duracion = findViewById(R.id.textView_3333);
        TextView tv_titulo_otorgado = findViewById(R.id.textView_4444);
        TextView tv_departamento_oferta_programa = findViewById(R.id.textView_5555);
        TextView tv_municipio_oferta_programa = findViewById(R.id.textView_6666);
        TextView tv_costo_matricula = findViewById(R.id.textView_7777);
        TextView tv_tiempo_admisiones_estudiantes = findViewById(R.id.textView_8888);
        ImageView tv_logouniversidad = findViewById(R.id.img_logo_u_detalles);




        tv_nombre_institucion.setText(nombre_institucion);
        tv_caracter_academico.setText("Caracter academico: "+caracter_academico);
        tv_codigo_snies.setText("Codigo snies: " + codigo_snies);
        tv_area_de_conococimiento.setText(area_de_conococimiento);
        tv_basico_de_conocimiento.setText(basico_de_conocimiento);
        tv_nombre_del_programa.setText(nombre_del_programa);
        tv_nivel_academico.setText(nivel_academico);
        tv_nivel_formacion.setText(nivel_formacion);
        tv_metodologia.setText(metodologia);
        tv_numero_periodos_de_duracion.setText(numero_periodos_de_duracion);
        tv_periodos_de_duracion.setText(periodos_de_duracion);
        tv_titulo_otorgado.setText(titulo_otorgado);
        tv_departamento_oferta_programa.setText(departamento_oferta_programa);
        tv_municipio_oferta_programa.setText(municipio_oferta_programa);
        tv_costo_matricula.setText(costo_matricula);
        tv_tiempo_admisiones_estudiantes.setText(tiempo_admisiones_estudiantes);

        Glide.with(this).load(logo_universidad).into(tv_logouniversidad);


    }


    public void  cordenadas(){

        String direccion_google = getIntent().getExtras().getString("Direccion google");

        String strUri = direccion_google;
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(strUri));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        startActivity(intent);
    }


}
