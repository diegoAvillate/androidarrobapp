package co.edu.arrobamedellin.arrobamedellin;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class SplashScreen extends AppCompatActivity {

    private ImageView iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        //---------------------------------------------------

        if(!isConnected(SplashScreen.this)) buildDialog(SplashScreen.this).show();
        else {
            //Toast.makeText(SplashScreen.this,"Welcome", Toast.LENGTH_SHORT).show();
            iv = (ImageView) findViewById(R.id.iv);
            Animation animation = AnimationUtils.loadAnimation(this,R.anim.mytransition);
            iv.startAnimation(animation);


            Thread splash=new Thread(){
                public void run(){

                    try{
                        sleep(3*1000);
                        Intent splash_intent=new Intent(getApplicationContext(), WelcomeActivity.class);
                        startActivity(splash_intent);
                        finish();
                    }catch (Exception e){

                    }
                }


            };
            splash.start();

        }

    }

    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting())) return true;
        else return false;
        } else
        return false;
    }

    public AlertDialog.Builder buildDialog(Context c) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("No hay conexión a internet");
        builder.setMessage("\n" + "Verifica tener datos móviles o wifi para acceder a esto. Presiona ok para salir");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                finish();
            }
        });

        return builder;
    }


}
