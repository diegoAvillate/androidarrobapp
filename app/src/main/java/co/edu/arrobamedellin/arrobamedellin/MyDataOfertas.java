package co.edu.arrobamedellin.arrobamedellin;

import android.graphics.drawable.Drawable;
import android.net.Uri;

import com.pchmn.materialchips.model.ChipInterface;

public class MyDataOfertas{

    private int id_universidad;
    private String codigo_institucion;
    private String nombre_institucion;
    private String naturaleza_juridica;
    private String sector;
    private String caracter_academico;
    private String departamento_domicilio;
    private String municipio_domicilio;
    private String direccion_domicilio;
    private String telefono_domicilio;
    private String estado;
    private String principal_seccional;
    private String pagina_web;
    private String acreditada_alta_calidad;
    private String fecha_acreditacion;
    private String resolucion;
    private String vigencia;
    private String logo_universidad;


    public MyDataOfertas(int id_universidad, String codigo_institucion, String nombre_institucion, String naturaleza_juridica, String sector, String caracter_academico, String departamento_domicilio, String municipio_domicilio, String direccion_domicilio, String telefono_domicilio, String estado, String principal_seccional, String pagina_web, String acreditada_alta_calidad, String fecha_acreditacion, String resolucion, String vigencia, String logo_universidad) {
        this.id_universidad = id_universidad;
        this.codigo_institucion = codigo_institucion;
        this.nombre_institucion = nombre_institucion;
        this.naturaleza_juridica = naturaleza_juridica;
        this.sector = sector;
        this.caracter_academico = caracter_academico;
        this.departamento_domicilio = departamento_domicilio;
        this.municipio_domicilio = municipio_domicilio;
        this.direccion_domicilio = direccion_domicilio;
        this.telefono_domicilio = telefono_domicilio;
        this.estado = estado;
        this.principal_seccional = principal_seccional;
        this.pagina_web = pagina_web;
        this.acreditada_alta_calidad = acreditada_alta_calidad;
        this.fecha_acreditacion = fecha_acreditacion;
        this.resolucion = resolucion;
        this.vigencia = vigencia;
        this.logo_universidad = logo_universidad;
    }

    public int getId_universidad() {
        return id_universidad;
    }

    public String getCodigo_institucion() {
        return codigo_institucion;
    }

    public String getNombre_institucion() {
        return nombre_institucion;
    }

    public String getNaturaleza_juridica() {
        return naturaleza_juridica;
    }

    public String getSector() {
        return sector;
    }

    public String getCaracter_academico() {
        return caracter_academico;
    }

    public String getDepartamento_domicilio() {
        return departamento_domicilio;
    }

    public String getMunicipio_domicilio() {
        return municipio_domicilio;
    }

    public String getDireccion_domicilio() {
        return direccion_domicilio;
    }

    public String getTelefono_domicilio() {
        return telefono_domicilio;
    }

    public String getEstado() {
        return estado;
    }

    public String getPrincipal_seccional() {
        return principal_seccional;
    }

    public String getPagina_web() {
        return pagina_web;
    }

    public String getAcreditada_alta_calidad() {
        return acreditada_alta_calidad;
    }

    public String getFecha_acreditacion() {
        return fecha_acreditacion;
    }

    public String getResolucion() {
        return resolucion;
    }

    public String getVigencia() {
        return vigencia;
    }

    public String getLogo_universidad() {
        return logo_universidad;
    }

    public void setId_universidad(int id_universidad) {
        this.id_universidad = id_universidad;
    }

    public void setCodigo_institucion(String codigo_institucion) {
        this.codigo_institucion = codigo_institucion;
    }

    public void setNombre_institucion(String nombre_institucion) {
        this.nombre_institucion = nombre_institucion;
    }

    public void setNaturaleza_juridica(String naturaleza_juridica) {
        this.naturaleza_juridica = naturaleza_juridica;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public void setCaracter_academico(String caracter_academico) {
        this.caracter_academico = caracter_academico;
    }

    public void setDepartamento_domicilio(String departamento_domicilio) {
        this.departamento_domicilio = departamento_domicilio;
    }

    public void setMunicipio_domicilio(String municipio_domicilio) {
        this.municipio_domicilio = municipio_domicilio;
    }

    public void setDireccion_domicilio(String direccion_domicilio) {
        this.direccion_domicilio = direccion_domicilio;
    }

    public void setTelefono_domicilio(String telefono_domicilio) {
        this.telefono_domicilio = telefono_domicilio;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setPrincipal_seccional(String principal_seccional) {
        this.principal_seccional = principal_seccional;
    }

    public void setPagina_web(String pagina_web) {
        this.pagina_web = pagina_web;
    }

    public void setAcreditada_alta_calidad(String acreditada_alta_calidad) {
        this.acreditada_alta_calidad = acreditada_alta_calidad;
    }

    public void setFecha_acreditacion(String fecha_acreditacion) {
        this.fecha_acreditacion = fecha_acreditacion;
    }

    public void setResolucion(String resolucion) {
        this.resolucion = resolucion;
    }

    public void setVigencia(String vigencia) {
        this.vigencia = vigencia;
    }

    public void setLogo_universidad(String logo_universidad) {
        this.logo_universidad = logo_universidad;
    }
}
