package co.edu.arrobamedellin.arrobamedellin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import co.edu.arrobamedellin.arrobamedellin.app.AppConfig;
import co.edu.arrobamedellin.arrobamedellin.app.AppController;
import co.edu.arrobamedellin.arrobamedellin.helper.SQLiteHandler;
import co.edu.arrobamedellin.arrobamedellin.helper.SessionManager;

import static com.android.volley.VolleyLog.TAG;

public class EditarDatosUsuario extends AppCompatActivity {

    private SQLiteHandler db;
    private SessionManager session;

    private EditText edtname, edtapellido ,edtcorreo, edtdocumento, edtfechanacimiento, edtinstitucion, edtprofesion, edtcelular;

    private ImageView imgeditar, imgcancelar;

    private Button guardar;

    String getname, getapellido, getdocumento, getemail, getuid,getcreate_at;

    Toolbar toolbar;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_datos_usuario);

        toolbar = (Toolbar) findViewById(R.id.toolbar_menu_pefil);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enter_Ofertas();
            }
        });

        imgeditar = (ImageView)findViewById(R.id.drop_down_option_editar);
        imgcancelar = (ImageView)findViewById(R.id.drop_down_option_cancelar);
        guardar = (Button)findViewById(R.id.edi_btn_guardar);
        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getname =edtname.getText().toString();
                getapellido = edtapellido.getText().toString();
                getdocumento = edtdocumento.getText().toString();
                getemail =  edtcorreo.getText().toString();

                db.updateUser(getname,getapellido,getdocumento,getemail);
                Toast.makeText(EditarDatosUsuario.this, "Datos actualizados", Toast.LENGTH_LONG).show();
            }
        });


        //txtname =(TextView)findViewById(R.id.abc_name);

        edtapellido = (EditText)findViewById(R.id.edi_apellido);
        edtname = (EditText) findViewById(R.id.edi_name);
        edtcorreo = (EditText) findViewById(R.id.edi_correo);
        edtdocumento = (EditText)findViewById(R.id.edi_documento);
        //edtfechanacimiento = (EditText)findViewById(R.id.edi_fecha_nacimiento);
        //edtinstitucion = (EditText) findViewById(R.id.edi_institucion);
        //edtprofesion = (EditText)findViewById(R.id.edi_profesion);
        //edtcelular = (EditText)findViewById(R.id.edi_celular);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());
        if (!session.isLoggedIn()) {

        }
        // Fetching user details from SQLite
        HashMap<String, String> user = db.getUserDetails();

        String name = user.get("name");
        String email = user.get("email");
        String apell = user.get("apell");
        String docu = user.get("docu");

        imgeditar.setImageDrawable(getDrawable(R.drawable.account_edit));


        //txtname.setText(name);
        edtname.setText(name);
        edtdocumento.setText(docu);
        edtapellido.setText(apell);

        edtapellido.setEnabled(false);
        edtname.setEnabled(false);
        edtcorreo.setText(email);
        edtcorreo.setEnabled(false);
        edtdocumento.setEnabled(false);
        guardar.setEnabled(false);
        guardar.setBackground(getDrawable(R.drawable.enablebotton));
        //edtfechanacimiento.setEnabled(false);
        //edtinstitucion.setEnabled(false);
        //edtprofesion.setEnabled(false);
        //edtcelular.setEnabled(false);

        imgeditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                edtname.setEnabled(true);
                edtapellido.setEnabled(true);
                edtcorreo.setEnabled(true);
                edtdocumento.setEnabled(true);
                //edtfechanacimiento.setEnabled(true);
                //edtinstitucion.setEnabled(true);
                //edtprofesion.setEnabled(true);
                //edtcelular.setEnabled(true);
                guardar.setEnabled(true);
                guardar.setBackground(getDrawable(R.drawable.buttonregistro));

                imgeditar.setEnabled(false);
                imgeditar.setVisibility(View.INVISIBLE);
                imgcancelar.setVisibility(View.VISIBLE);
                imgcancelar.setImageDrawable(getDrawable(R.drawable.ic_cancel_white_24dp));

            }
        });

        imgcancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                edtname.setEnabled(false);
                edtapellido.setEnabled(false);
                edtcorreo.setEnabled(false);
                edtdocumento.setEnabled(false);
                //edtfechanacimiento.setEnabled(false);
                //edtinstitucion.setEnabled(false);
                //edtprofesion.setEnabled(false);
                //edtcelular.setEnabled(false);

                imgeditar.setEnabled(true);
                imgeditar.setVisibility(View.VISIBLE);
                imgcancelar.setVisibility(View.INVISIBLE);

                guardar.setEnabled(false);
                guardar.setBackground(getDrawable(R.drawable.enablebotton));
            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            Enter_Ofertas();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void Enter_Ofertas() {
        Intent intent = new Intent(this, PerfilUsuario.class);
        startActivity(intent);
        finish();

    }

}
