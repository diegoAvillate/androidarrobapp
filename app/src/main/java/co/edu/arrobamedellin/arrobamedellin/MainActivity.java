package co.edu.arrobamedellin.arrobamedellin;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import co.edu.arrobamedellin.arrobamedellin.helper.SQLiteHandler;
import co.edu.arrobamedellin.arrobamedellin.helper.SessionManager;

public class MainActivity extends AppCompatActivity  {

    private Toolbar appbar;
    private DrawerLayout drawerLayout;
    private NavigationView navView;

    private static final int INTERVALO = 2000; //2 segundos para salir
    private long tiempoPrimerClick;

    private TextView txtName;
    private TextView txtperfil, txttes, txtnovedades, txtofertas, txtsobrenotros, txtsalir;

    Button btn_test_vocacional;
    Button btn_novedades;
    Button btn_ofertas;

    private SQLiteHandler db;
    private SessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        appbar = (Toolbar)findViewById(R.id.appbar);
        setSupportActionBar(appbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_nav_menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navview);
        View view = navigationView.getHeaderView(0);
        txtName = (TextView)view. findViewById(R.id.name_ac);
        txtperfil = (TextView)view.findViewById(R.id.peril_menu_principal);
        txttes = (TextView)view.findViewById(R.id.test_menu_principal);
        txtnovedades = (TextView)view.findViewById(R.id.novedades_menu_principal);
        txtofertas = (TextView)view.findViewById(R.id.ofertas_menu_princiapl);
        txtsobrenotros = (TextView)view.findViewById(R.id.sobrenosotros_menu_principal);
        txtsalir = (TextView)view.findViewById(R.id.salir_menu_principal);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());
        if (!session.isLoggedIn()) {

        }


        // Fetching user details from SQLite
        HashMap<String, String> user = db.getUserDetails();

        String name = user.get("name");

        // Displaying the user details on the screen
        txtName.setText(name);

        //------items menu principal----------

        txtperfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enter_Perfil();
            }
        });

        txttes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enter_test_vocacinal();
            }
        });

        txtnovedades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enter_Novedades();
                finish();
            }
        });

        txtofertas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enter_ofertas();
            }
        });

        txtsobrenotros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sobre_nosotros();
            }
        });

        txtsalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Salir de @Medellín");
                builder.setMessage("¿ Desea salir de esta aplicación ?");
                builder.setPositiveButton("Salir", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("EXIT", true);
                        finishAffinity();

                    }
                });
                builder.setNegativeButton(android.R.string.cancel, null);
                Dialog dialog = builder.create();
                dialog.show();

            }
        });


        //------------------------------------------------

        btn_test_vocacional = (Button) findViewById(R.id.tas1);
        btn_test_vocacional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (session.isLoggedIn()){

                    Test_vocacional();
                    finish();

                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("No hay un usuario registrado");
                    builder.setMessage("¿ Desea hacer el registro ?");
                    builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Enter_registro();

                        }
                    });
                    builder.setNegativeButton(android.R.string.cancel, null);
                    Dialog dialog = builder.create();
                    dialog.show();
                }
            }
        });
        btn_novedades = (Button) findViewById(R.id.tas2);
        btn_novedades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enter_Novedades();
                finish();
            }
        });
        btn_ofertas = (Button) findViewById(R.id.tas3);
        btn_ofertas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (session.isLoggedIn()){

                    Enter_ofertas();
                    finish();

                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("No hay un usuario registrado");
                    builder.setMessage("¿ Desea hacer el registro ?");
                    builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Enter_registro();

                        }
                    });
                    builder.setNegativeButton(android.R.string.cancel, null);
                    Dialog dialog = builder.create();
                    dialog.show();
                }

            }
        });

        //---------------------------------------------------------

        navView = (NavigationView)findViewById(R.id.navview);
        navView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        int id = menuItem.getItemId();

                        if (id == R.id.menu_seccion_perfil) {


                        } else if (id == R.id.menu_seccion_test_vocacional) {


                        } else if (id == R.id.menu_seccion_ofertas_novedades) {


                        } else if (id == R.id.menu_seccion_ofertas_ofertas) {


                        } else if (id == R.id.menu_opcion_sobre_nosotros) {



                        } else if (id == R.id.menu_opcion_salir) {



                        }

                            drawerLayout.closeDrawers();

                        return true;
                    }
                });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch(item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }



    private void Enter_test_vocacinal() {
        if (session.isLoggedIn()){

            Test_vocacional();
            finish();

        }else {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("No hay un usuario registrado");
            builder.setMessage("¿ Desea hacer el registro ?");
            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Enter_registro();
                    finish();

                }
            });
            builder.setNegativeButton(android.R.string.cancel, null);
            Dialog dialog = builder.create();
            dialog.show();
        }
    }

    private void Enter_Novedades() {
        Intent intent = new Intent(this, Novedades.class);
        startActivity(intent);
    }

    private void Enter_ofertas() {
        if (session.isLoggedIn()){

            Ofertas();
            finish();

        }else {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("No hay un usuario registrado");
            builder.setMessage("¿ Desea hacer el registro ?");
            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Enter_registro();
                    finish();

                }
            });
            builder.setNegativeButton(android.R.string.cancel, null);
            Dialog dialog = builder.create();
            dialog.show();
        }
    }
    private void Enter_Perfil() {

        if (session.isLoggedIn()){

            Intent intent = new Intent(this, PerfilUsuario.class);
            startActivity(intent);
            finish();

        }else {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("No hay un usuario registrado");
            builder.setMessage("¿ Desea hacer el registro ?");
            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Enter_registro();
                    finish();

                }
            });
            builder.setNegativeButton(android.R.string.cancel, null);
            Dialog dialog = builder.create();
            dialog.show();
        }

    }
    private void Sobre_nosotros(){
        Intent intent =new Intent(this, SobreNosotros.class);
        startActivity(intent);
        finish();
    }
    private void Enter_registro(){
        Intent intent =new Intent(this, RegistroUsuario.class);
        startActivity(intent);
        finish();
    }
    private void Test_vocacional(){
        Intent intent =new Intent(this, TestVocacional.class);
        startActivity(intent);
        finish();
    }
    private void Ofertas(){
        Intent intent =new Intent(this, Ofertas.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed(){
        if (tiempoPrimerClick + INTERVALO > System.currentTimeMillis()){
            super.onBackPressed();
            finishAffinity();
            return;
        }else {
            Toast.makeText(this, "Vuelve a presionar para salir", Toast.LENGTH_SHORT).show();
        }
        tiempoPrimerClick = System.currentTimeMillis();
    }

}
