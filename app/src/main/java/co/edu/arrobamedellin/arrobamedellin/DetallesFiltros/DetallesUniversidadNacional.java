package co.edu.arrobamedellin.arrobamedellin.DetallesFiltros;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import co.edu.arrobamedellin.arrobamedellin.AdapterFiltros.AdapterContentUniversidades;
import co.edu.arrobamedellin.arrobamedellin.DatosFiltros.DatosContentUniversidades;
import co.edu.arrobamedellin.arrobamedellin.Filtros.FiltroAConocimiento;
import co.edu.arrobamedellin.arrobamedellin.Filtros.FiltroUniversiades;
import co.edu.arrobamedellin.arrobamedellin.Ofertas;
import co.edu.arrobamedellin.arrobamedellin.R;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class DetallesUniversidadNacional extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private RecyclerView recyclerView;
    private GridLayoutManager gridLayoutManager;
    private AdapterContentUniversidades adapter;
    private List<DatosContentUniversidades> data_list;
    View ChildView;
    int GetItemPosition ;
    ArrayList<String> Mydata;

    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_universidad_nacional);

        toolbar = (Toolbar) findViewById(R.id.toolbar_buscar);
        setSupportActionBar(toolbar);


        //-----------servicios web ----------------------------------

        String codigo_institucion = getIntent().getExtras().getString("codigo_institucion");




        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando ofertas...");
        progressDialog.show();

        recyclerView = (RecyclerView) findViewById(R.id.ofertas_in_filtros_nacional);
        data_list = new ArrayList<>();
        load_data_from_server(0);

        gridLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(gridLayoutManager);

        adapter = new AdapterContentUniversidades(this, data_list);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {


                if (gridLayoutManager.findLastCompletelyVisibleItemPosition() == data_list.size()) {

                    load_data_from_server(data_list.get(data_list.size()).getId_ies_medellin());
                    progressDialog.dismiss();
                }

                progressDialog.dismiss();

            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {

            GestureDetector gestureDetector = new GestureDetector(DetallesUniversidadNacional.this, new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onSingleTapUp(MotionEvent motionEvent) {

                    return true;
                }

            });

            @Override
            public boolean onInterceptTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {

                ChildView = Recyclerview.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                if (ChildView != null && gestureDetector.onTouchEvent(motionEvent)) {

                    GetItemPosition = Recyclerview.getChildAdapterPosition(ChildView);


                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    private void load_data_from_server(final int id) {


        @SuppressLint("StaticFieldLeak") AsyncTask<Integer,Void,Void> task = new AsyncTask<Integer, Void, Void>() {

            @Override
            protected Void doInBackground(Integer... integers) {

                String Url = null;
                String codigo_institucion = getIntent().getExtras().getString("codigo_institucion");
                Url=("http://200.13.254.146/webserviceapp/filtros/detalle_universidad.php?codigo_universidad="+codigo_institucion);

//                switch (codigo_institucion){
//                    case"2110":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_mayor_antioquia.php");
//                        break;
//                    case"4801":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_academia_de_artes.php");
//                        break;
//                    case"3820":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_atec.php");
//                        break;
//                    case"2838":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_colegiatura_colombiana.php");
//                        break;
//                    case"4827":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_superior_de_artes.php");
//                        break;
//                    case"2815":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_adventista_unac.php");
//                        break;
//                    case"2833":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_remington.php");
//                        break;
//                    case"9900":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_u_colombia.php");
//                        break;
//                    case"3807":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_eta.php");
//                        break;
//                    case"3703":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_escolme.php");
//                        break;
//                    case"2747":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_autonoma_de_las_americas.php");
//                        break;
//                    case"9120":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_bellas_artes.php");
//                        break;
//                    case"3720":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_esumer.php");
//                        break;
//                    case"2721":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_maria_cano.php");
//                        break;
//                    case"2736":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_seminario_biblico.php");
//                        break;
//                    case"2749":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_salazar_y_herrera.php");
//                        break;
//                        case"3107":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_pascula_bravo.php");
//                        break;
//                    case"3302":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_tecnologico_metropolitano.php");
//                        break;
//                    case"2209":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_jaime_isaza.php");
//                        break;
//                    case"9112":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_tecnologica_sena.php");
//                        break;
//                    case"3204":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_tecnologica_de_antioquia.php");
//                        break;
//                    case"1814":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_autonoma_latinoamericana.php");
//                        break;
//                    case"2719":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_catolica_luis_amigo.php");
//                        break;
//                    case"2708":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_ces.php");
//                        break;
//                    case"1816":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_cooperativa_de_colombia.php");
//                        break;
//                    case"1201":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_de_antioquia.php");
//                        break;
//                    case"1812":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_de_medellin.php");
//                        break;
//                    case"1717":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_de_san_buenaventura.php");
//                        break;
//                    case"1712":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/universidad_eafit.php");
//                        break;
//                    case"1102":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/univercidad_nacional_de_colombia.php");
//                        break;
//                    case"1710":
//                        Url=("http://200.13.254.146/webserviceapp/filtros/universidad_pontificia_bolivariana.php");
//                        break;
//
//                         default:
//                             Toast toast1 = Toast.makeText(getApplicationContext(), "Eror de ip", Toast.LENGTH_SHORT);
//                             toast1.show();
//
//                }

                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(Url)
                        .build();


                try {
                    okhttp3.Response response = client.newCall(request).execute();


                    JSONArray array = new JSONArray(response.body().string());

                    for (int i=0; i<array.length(); i++){

                        JSONObject object = array.getJSONObject(i);

                        DatosContentUniversidades data_ofertas = new DatosContentUniversidades(
                                object.getInt("id_ies_medellin"),
                                object.getString("codigo_institucion"),
                                object.getString("nombre_institucion"),
                                object.getString("estado_institucion"),
                                object.getString("caracter_academico"),
                                object.getString("sector"),
                                object.getString("codigo_snies"),
                                object.getString("area_de_conococimiento"),
                                object.getString("basico_de_conocimiento"),
                                object.getString("nombre_del_programa"),
                                object.getString("nivel_academico"),
                                object.getString("nivel_formacion"),
                                object.getString("metodologia"),
                                object.getString("numero_periodos_de_duracion"),
                                object.getString("periodos_de_duracion"),
                                object.getString("titulo_otorgado"),
                                object.getString("departamento_oferta_programa"),
                                object.getString("municipio_oferta_programa"),
                                object.getString("costo_matricula"),
                                object.getString("tiempo_admisiones_estudiantes"),
                                object.getString("Direccion google"),
                                object.getString("Logo universidad"));

                        data_list.add(data_ofertas);
                    }



                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    System.out.println("End of content");
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                adapter.notifyDataSetChanged();
            }
        };

        task.execute(id);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_filtros,menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        newText = newText.toLowerCase();
        ArrayList<DatosContentUniversidades> newList = new ArrayList<>();

        for (DatosContentUniversidades datosContentUniversidades : data_list){
            String name = datosContentUniversidades.getNombre_del_programa().toLowerCase();
            if(name.contains(newText)){
                newList.add(datosContentUniversidades);
            }

        }
        adapter.setFilter(newList);


        return true;
    }
}
