package co.edu.arrobamedellin.arrobamedellin.DatosFiltros;

import android.graphics.drawable.Drawable;
import android.net.Uri;

public class DatosContentUniversidades {

    private int id_ies_medellin;
    private String codigo_institucion;
    private String nombre_institucion;
    private String estado_institucion;
    private String caracter_academico;
    private String sector;
    private String codigo_snies;
    private String area_de_conococimiento;
    private String basico_de_conocimiento;
    private String nombre_del_programa;
    private String nivel_academico;
    private String nivel_formacion;
    private String metodologia;
    private String numero_periodos_de_duracion;
    private String periodos_de_duracion;
    private String titulo_otorgado;
    private String departamento_oferta_programa;
    private String municipio_oferta_programa;
    private String costo_matricula;
    private String tiempo_admisiones_estudiantes;
    private String direccion_google;
    private String logo_universidad;

    public DatosContentUniversidades(int id_ies_medellin,
                         String codigo_institucion,
                         String nombre_institucion,
                         String estado_institucion,
                         String caracter_academico,
                         String sector,
                         String codigo_snies,
                         String area_de_conococimiento,
                         String basico_de_conocimiento,
                         String nombre_del_programa,
                         String nivel_academico,
                         String nivel_formacion,
                         String metodologia,
                         String numero_periodos_de_duracion,
                         String periodos_de_duracion,
                         String titulo_otorgado,
                         String departamento_oferta_programa,
                         String municipio_oferta_programa,
                         String costo_matricula,
                         String tiempo_admisiones_estudiantes,
                         String direccion_google,
                         String logo_universidad){

        this.id_ies_medellin = id_ies_medellin;
        this.codigo_institucion = codigo_institucion;
        this.nombre_institucion = nombre_institucion;
        this.estado_institucion = estado_institucion;
        this.caracter_academico = caracter_academico;
        this.sector = sector;
        this.codigo_snies = codigo_snies;
        this.area_de_conococimiento = area_de_conococimiento;
        this.basico_de_conocimiento = basico_de_conocimiento;
        this.nombre_del_programa = nombre_del_programa;
        this.nivel_academico = nivel_academico;
        this.nivel_formacion  = nivel_formacion;
        this.metodologia = metodologia;
        this.numero_periodos_de_duracion = numero_periodos_de_duracion;
        this.periodos_de_duracion = periodos_de_duracion;
        this.titulo_otorgado = titulo_otorgado;
        this.departamento_oferta_programa = departamento_oferta_programa;
        this.municipio_oferta_programa = municipio_oferta_programa;
        this.costo_matricula = costo_matricula;
        this.tiempo_admisiones_estudiantes = tiempo_admisiones_estudiantes;
        this.direccion_google = direccion_google;
        this.logo_universidad = logo_universidad;

    }

    public int getId_ies_medellin(){
        return id_ies_medellin;
    }

    public void setId_ies_medellin(int id_ies_medellin){
        this.id_ies_medellin = id_ies_medellin;
    }

    //----------------------------------------------------------

    public String getCodigo_institucion() {
        return codigo_institucion;
    }
    public void setCodigo_institucion(String codigo_institucion){
        this.codigo_institucion = codigo_institucion;
    }

    //----------------------------------------------------------

    public String getNombre_institucion() {
        return nombre_institucion;
    }
    public void setNombre_institucion(String nombre_institucion){
        this.nombre_institucion = nombre_institucion;
    }

    //----------------------------------------------------------

    public String getEstado_institucion() {
        return estado_institucion;
    }
    public void setEstado_institucion(String estado_institucion){
        this.estado_institucion = estado_institucion;
    }

    //----------------------------------------------------------

    public String getCaracter_academico() {
        return caracter_academico;
    }
    public void setCaracter_academico(String caracter_academico){
        this.caracter_academico = caracter_academico;
    }

    //----------------------------------------------------------

    public String getSector() {
        return sector;
    }
    public void setSector(String sector){
        this.sector = sector;
    }

    //----------------------------------------------------------

    public String getCodigo_snies() {
        return codigo_snies;
    }
    public void setCodigo_snies(String codigo_snies){
        this.codigo_snies = codigo_snies;
    }

    //----------------------------------------------------------

    public String getArea_de_conococimiento() {
        return area_de_conococimiento;
    }
    public void setArea_de_conococimiento(String area_de_conococimiento){
        this.area_de_conococimiento = area_de_conococimiento;
    }

    //----------------------------------------------------------

    public String getBasico_de_conocimiento() {
        return basico_de_conocimiento;
    }
    public void setBasico_de_conocimiento(String basico_de_conocimiento){
        this.basico_de_conocimiento = basico_de_conocimiento;
    }

    //----------------------------------------------------------

    public String getNombre_del_programa() {
        return nombre_del_programa;
    }
    public void setNombre_del_programa(String nombre_del_programa){
        this.nombre_del_programa = nombre_del_programa;
    }

    //----------------------------------------------------------

    public String getNivel_academico() {
        return nivel_academico;
    }
    public void setNivel_academico(String nivel_academico){
        this.nivel_academico = nivel_academico;
    }

    //----------------------------------------------------------

    public String getNivel_formacion() {
        return nivel_formacion;
    }
    public void setNivel_formacion(String nivel_formacion){
        this.nivel_formacion = nivel_formacion;
    }

    //----------------------------------------------------------

    public String getMetodologia() {
        return metodologia;
    }
    public void setMetodologia(String metodologia){
        this.metodologia = metodologia;
    }

    //----------------------------------------------------------

    public String getNumero_periodos_de_duracion() {
        return numero_periodos_de_duracion;
    }
    public void setNumero_periodos_de_duracion(String numero_periodos_de_duracion){
        this.numero_periodos_de_duracion = numero_periodos_de_duracion;
    }

    //----------------------------------------------------------

    public String getPeriodos_de_duracion() {
        return periodos_de_duracion;
    }
    public void setPeriodos_de_duracion(String periodos_de_duracion){
        this.periodos_de_duracion = periodos_de_duracion;
    }

    //----------------------------------------------------------

    public String getTitulo_otorgado() {
        return titulo_otorgado;
    }
    public void setTitulo_otorgado(String titulo_otorgado){
        this.titulo_otorgado = titulo_otorgado;
    }

    //----------------------------------------------------------

    public String getDepartamento_oferta_programa() {
        return departamento_oferta_programa;
    }
    public void setDepartamento_oferta_programa(String departamento_oferta_programa){
        this.departamento_oferta_programa = departamento_oferta_programa;
    }

    //----------------------------------------------------------

    public String getMunicipio_oferta_programa() {
        return municipio_oferta_programa;
    }
    public void setMunicipio_oferta_programa(String municipio_oferta_programa){
        this.municipio_oferta_programa = municipio_oferta_programa;
    }

    //----------------------------------------------------------

    public String getCosto_matricula() {
        return costo_matricula;
    }
    public void setCosto_matricula(String costo_matricula){
        this.costo_matricula = costo_matricula;
    }

    //----------------------------------------------------------

    public String getTiempo_admisiones_estudiantes() {
        return tiempo_admisiones_estudiantes;
    }
    public void setTiempo_admisiones_estudiantes(String tiempo_admisiones_estudiantes){
        this.tiempo_admisiones_estudiantes = tiempo_admisiones_estudiantes;
    }

    //--------------------------------------------------------------


    public String getDireccion_google() {
        return direccion_google;
    }

    public void setDireccion_google(String direccion_google) {
        this.direccion_google = direccion_google;
    }

    //---------------------------------------------------------------

    public String getLogo_universidad() {
        return logo_universidad;
    }

    public void setLogo_universidad(String logo_universidad) {
        this.logo_universidad = logo_universidad;
    }
}
