package co.edu.arrobamedellin.arrobamedellin;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import co.edu.arrobamedellin.arrobamedellin.AdapterFiltros.AdapterContentUniversidades;
import co.edu.arrobamedellin.arrobamedellin.DatosFiltros.DatosContentUniversidades;
import co.edu.arrobamedellin.arrobamedellin.DetallesFiltros.DetallesUniversidadNacional;

public class CustomAdapterOfertas extends RecyclerView.Adapter<CustomAdapterOfertas.ViewHolder> {

    private Context context;
    private List<MyDataOfertas> my_data_ofertas;


    public CustomAdapterOfertas(Context context, List<MyDataOfertas> my_data_ofertas) {
        this.context = context;
        this.my_data_ofertas = my_data_ofertas;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater minflater = LayoutInflater.from(context);
        view = minflater.inflate(R.layout.card_ofertas,parent,false);
        final ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {


        holder.nombre_univercidad.setText(my_data_ofertas.get(position).getNombre_institucion());
        holder.view_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent u = new Intent(context, DetallesUniversidadNacional.class);
                u.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
                context.startActivity(u);

//                switch (my_data_ofertas.get(position).getId_universidad()){
//                    case 1:
//                        Intent u2 = new Intent(context, DetallesUniversidadNacional.class);
//                        u2.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u2);
//                        break;
//                    case 2:
//                        Intent u3 = new Intent(context, DetallesUniversidadNacional.class);
//                        u3.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u3);
//                        break;
//                    case 3:
//                        Intent u4 = new Intent(context, DetallesUniversidadNacional.class);
//                        u4.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u4);
//                        break;
//                    case 4:
//                        Intent u5 = new Intent(context, DetallesUniversidadNacional.class);
//                        u5.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u5);
//                        break;
//                    case 5:
//                        Intent u6 = new Intent(context, DetallesUniversidadNacional.class);
//                        u6.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u6);
//                        break;
//                    case 6:
//                        Intent u7 = new Intent(context, DetallesUniversidadNacional.class);
//                        u7.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u7);
//                        break;
//                    case 7:
//                        Intent u8 = new Intent(context, DetallesUniversidadNacional.class);
//                        u8.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u8);
//                        break;
//                    case 8:
//                        Intent u9 = new Intent(context, DetallesUniversidadNacional.class);
//                        u9.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u9);
//                        break;
//                    case 9:
//                        Intent u10 = new Intent(context, DetallesUniversidadNacional.class);
//                        u10.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u10);
//                        break;
//                    case 10:
//                        Intent u11 = new Intent(context, DetallesUniversidadNacional.class);
//                        u11.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u11);
//                        break;
//                    case 11:
//                        Intent u12 = new Intent(context, DetallesUniversidadNacional.class);
//                        u12.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u12);
//                        break;
//                    case 12:
//                        Intent u13 = new Intent(context, DetallesUniversidadNacional.class);
//                        u13.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u13);
//                        break;
//                    case 13:
//                        Intent u14 = new Intent(context, DetallesUniversidadNacional.class);
//                        u14.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u14);
//                        break;
//                    case 14:
//                        Intent u15 = new Intent(context, DetallesUniversidadNacional.class);
//                        u15.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u15);
//                        break;
//                    case 15:
//                        Intent u16 = new Intent(context, DetallesUniversidadNacional.class);
//                        u16.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u16);
//                        break;
//                    case 16:
//                        Intent u17 = new Intent(context, DetallesUniversidadNacional.class);
//                        u17.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u17);
//                        break;
//                    case 17:
//                        Intent u18 = new Intent(context, DetallesUniversidadNacional.class);
//                        u18.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u18);
//                        break;
//                    case 18:
//                        Intent u19 = new Intent(context, DetallesUniversidadNacional.class);
//                        u19.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u19);
//                        break;
//                    case 19:
//                        Intent u20 = new Intent(context, DetallesUniversidadNacional.class);
//                        u20.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u20);
//                        break;
//                    case 20:
//                        Intent u21 = new Intent(context, DetallesUniversidadNacional.class);
//                        u21.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u21);
//                        break;
//                    case 21:
//                        Intent u22 = new Intent(context, DetallesUniversidadNacional.class);
//                        u22.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u22);
//                        break;
//                    case 22:
//                        Intent u23 = new Intent(context, DetallesUniversidadNacional.class);
//                        u23.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u23);
//                        break;
//                    case 23:
//                        Intent u24 = new Intent(context, DetallesUniversidadNacional.class);
//                        u24.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u24);
//                        break;
//                    case 24:
//                        Intent u25 = new Intent(context, DetallesUniversidadNacional.class);
//                        u25.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u25);
//                        break;
//                    case 25:
//                        Intent u26 = new Intent(context, DetallesUniversidadNacional.class);
//                        u26.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u26);
//                        break;
//                    case 26:
//                        Intent u27 = new Intent(context, DetallesUniversidadNacional.class);
//                        u27.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u27);
//                        break;
//                    case 27:
//                        Intent u28 = new Intent(context, DetallesUniversidadNacional.class);
//                        u28.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u28);
//                        break;
//                    case 28:
//                        Intent u29 = new Intent(context, DetallesUniversidadNacional.class);
//                        u29.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u29);
//                        break;
//                    case 29:
//                        Intent u30 = new Intent(context, DetallesUniversidadNacional.class);
//                        u30.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u30);
//                        break;
//                    case 30:
//                        Intent u31 = new Intent(context, DetallesUniversidadNacional.class);
//                        u31.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u31);
//                        break;
//                    case 31:
//                        Intent u32 = new Intent(context, DetallesUniversidadNacional.class);
//                        u32.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u32);
//                        break;
//                    case 32:
//                        Intent u33 = new Intent(context, DetallesUniversidadNacional.class);
//                        u33.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u33);
//                        break;
//                    case 33:
//                        Intent u34 = new Intent(context, DetallesUniversidadNacional.class);
//                        u34.putExtra("codigo_institucion",my_data_ofertas.get(position).getCodigo_institucion());
//                        context.startActivity(u34);
//                        break;
//
//                    default:
//
//                }
            }
        });

        holder.sector.setText("Sector: " + my_data_ofertas.get(position).getSector());
        Glide.with(context).load(my_data_ofertas.get(position).getLogo_universidad()).into(holder.logo_universidad);



    }

    @Override
    public int getItemCount() {
        return my_data_ofertas.size();
    }

    public  class ViewHolder extends  RecyclerView.ViewHolder{


        public TextView nombre_univercidad;
        public TextView sector;
        public ImageView logo_universidad;

        LinearLayout view_container;


        public ViewHolder(View itemView) {
            super(itemView);
            nombre_univercidad= (TextView) itemView.findViewById(R.id.txt_nombre_universidad);
            sector =(TextView) itemView.findViewById(R.id.txt_sector);
            logo_universidad = (ImageView)itemView.findViewById(R.id.img_logo_universidad);
            view_container = itemView.findViewById(R.id.container_ofertas);
        }

    }

    public  void setFilter(ArrayList<MyDataOfertas> newlist){

        my_data_ofertas = new ArrayList<>();
        my_data_ofertas.addAll(newlist);
        notifyDataSetChanged();

    }
}
