package co.edu.arrobamedellin.arrobamedellin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

import co.edu.arrobamedellin.arrobamedellin.helper.SQLiteHandler;
import co.edu.arrobamedellin.arrobamedellin.helper.SessionManager;

public class TestVocacional extends AppCompatActivity {


    private SQLiteHandler db;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_vocacional);


        db = new SQLiteHandler(getApplicationContext());
        session = new SessionManager(getApplicationContext());
        if (!session.isLoggedIn()) {

        }
        HashMap<String, String> user = db.getUserDetails();

        String name = user.get("name");
        String email = user.get("email");
        String apell = user.get("apell");
        String docu = user.get("docu");

        WebView myWebView = (WebView) findViewById(R.id.webview_test_vocacional);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.setWebViewClient(new WebViewClient());
//        try {
//
//            String n = name;
//            String a = apell;
//            String d = docu;
//            String f = "2018";
//            String e = email;
//            String i = "institucion";
//            String g = "grado";
//            String c = "celular";
//
//            myWebView.loadUrl("http://campusdigital.arrobamedellin.edu.co/campus/estudiante/vocacionalMovil/index.php?n="
//                    + URLEncoder.encode(n,"UTF-8")+ "&a="
//                    + URLEncoder.encode(a,"UTF-8")+ "&d="
//                    + URLEncoder.encode(d,"UTF-8")+ "&f="
//                    + URLEncoder.encode(f,"UTF-8")+ "&e="
//                    + URLEncoder.encode(e,"UTF-8")+ "&i="
//                    + URLEncoder.encode(i,"UTF-8")+ "&g="
//                    + URLEncoder.encode(g,"UTF-8")+ "&c="
//                    + URLEncoder.encode(c,"UTF-8")
//            );
//
//
//
//        }catch (UnsupportedEncodingException ex) {
//            ex.printStackTrace();
//        }
        myWebView.getSettings().setUseWideViewPort(true);
        myWebView.getSettings().setLoadWithOverviewMode(true);
        myWebView.loadUrl("http://siguientenivel.com.co/");
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            Enter_login();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void Enter_login(){
        Intent intent =new Intent(this, RegistroUsuario.class);
        startActivity(intent);
    }
}
