package co.edu.arrobamedellin.arrobamedellin;

import android.content.Context;
import android.content.Intent;
import android.renderscript.Long4;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import java.util.List;

public class CustomAdapterNovedades extends RecyclerView.Adapter<CustomAdapterNovedades.ViewHolder> {

    private Context context;
    private List<MyDataNovedades> my_data;




    public CustomAdapterNovedades(Context context, List<MyDataNovedades> my_data) {
        this.context = context;
        this.my_data = my_data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        

        View view;
        LayoutInflater minflater = LayoutInflater.from(context);
        view = minflater.inflate(R.layout.card_novedades,parent,false);

        final ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.view_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DetallesNovedades.class);

                i.putExtra("titulo",my_data.get(viewHolder.getAdapterPosition()).getTitulo());
                i.putExtra("contenido",my_data.get(viewHolder.getAdapterPosition()).getContenido());
                i.putExtra("resumen",my_data.get(viewHolder.getAdapterPosition()).getResumen());
                i.putExtra("fecha",my_data.get(viewHolder.getAdapterPosition()).getFecha());
                i.putExtra("imagen",my_data.get(viewHolder.getAdapterPosition()).getImagen());
                i.putExtra("link", my_data.get(viewHolder.getAdapterPosition()).getLink());

                context.startActivity(i);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.titulo.setText(my_data.get(position).getTitulo());
        holder.resumen.setText(my_data.get(position).getResumen());
        holder.fecha.setText(my_data.get(position).getFecha());
        Glide.with(context).load(my_data.get(position).getImagen()).into(holder.imagen);

    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public  class ViewHolder extends  RecyclerView.ViewHolder{

        public TextView titulo;
        public TextView resumen;
        public TextView fecha;
        public ImageView imagen;
        LinearLayout view_container;


        public ViewHolder(View itemView) {
            super(itemView);

            titulo = (TextView) itemView.findViewById(R.id.txt_titulo);
            resumen= (TextView) itemView.findViewById(R.id.txt_resumen);
            fecha = (TextView) itemView.findViewById(R.id.txt_fecha);
            imagen = (ImageView) itemView.findViewById(R.id.imageView_1);

            view_container = itemView.findViewById(R.id.container_novedades);


        }
    }
}
