package co.edu.arrobamedellin.arrobamedellin;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;

import co.edu.arrobamedellin.arrobamedellin.helper.SQLiteHandler;
import co.edu.arrobamedellin.arrobamedellin.helper.SessionManager;

public class PerfilUsuario extends Activity {

    private TextView txtName;
    private TextView txtEmail;
    private ImageView btnLogout;
    private ImageView btnSobrenosotros;

    private TextView gotest, gonovedades, goofertas, gohome, logout, misdatos;

    private SQLiteHandler db;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_usuario);

        txtName = (TextView) findViewById(R.id.ab_name);
        txtEmail = (TextView) findViewById(R.id.ab_mail);
        btnLogout = (ImageView) findViewById(R.id.drop_down_option_menu);
        //btnSobrenosotros = (ImageView) findViewById(R.id.drop_down_option_menu2);
        // gotest = (TextView)findViewById(R.id.perfil_test_vocacional);
        //gonovedades =(TextView)findViewById(R.id.perfil_novedades);
        //goofertas = (TextView)findViewById(R.id.perfil_ofertas);
        gohome = (TextView)findViewById(R.id.perfil_home);
        logout = (TextView)findViewById(R.id.cerrar_perfil);
        misdatos =(TextView)findViewById(R.id.mis_datos);

        //-------------------session manager-------------------------------------

        db = new SQLiteHandler(getApplicationContext());
        session = new SessionManager(getApplicationContext());
        if (!session.isLoggedIn()) {

        }
        HashMap<String, String> user = db.getUserDetails();

        String name = user.get("name");
        String email = user.get("email");

        // Displaying the user details on the screen
        txtName.setText(name);
        txtEmail.setText(email);


        //-------------------Evento botones.---------------------------------------

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });

        gotest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enter_test_vocacional();
                finish();
            }
        });

        gonovedades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enter_novedades();
                finish();
            }
        });

        goofertas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enter_ofertas();
                finish();
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });

        misdatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enter_mis_datos();
            }
        });


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            Enter_login();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void logoutUser() {


        // Launching the login activity
        final AlertDialog.Builder builder = new AlertDialog.Builder(PerfilUsuario.this);
        builder.setTitle("Alerta");
        builder.setMessage("¿Desea cerrar sesión en esta aplicacion?");
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                session.setLogin(false);
                db.deleteUsers();
                Enter_home();
                finish();

            }
        });
        builder.setNegativeButton(android.R.string.cancel, null);
        Dialog dialog = builder.create();
        dialog.show();

    }

    private void Enter_login(){
        Intent intent =new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void Enter_test_vocacional(){
        Intent intent =new Intent(this, RegistroUsuario.class);
        startActivity(intent);
    }

    private void Enter_novedades(){
        Intent intent =new Intent(this, Novedades.class);
        startActivity(intent);
    }

    private void Enter_ofertas(){
        Intent intent =new Intent(this, Ofertas.class);
        startActivity(intent);
    }
    private void Sobre_nosotros(){
        Intent intent =new Intent(this, SobreNosotros.class);
        startActivity(intent);
        finish();
    }

    private void Enter_home(){
        Intent intent =new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
    private void Enter_mis_datos(){
        Intent intent =new Intent(this, EditarDatosUsuario.class);
        startActivity(intent);
        finish();
    }

}


