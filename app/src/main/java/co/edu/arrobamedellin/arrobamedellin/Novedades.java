package co.edu.arrobamedellin.arrobamedellin;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.pchmn.materialchips.R2;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.edu.arrobamedellin.arrobamedellin.helper.SessionManager;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Novedades extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    NotificationCompat.Builder notificacion;

    private RecyclerView recyclerView_horizontal;
    private CustomAdapterNhorizontal adapter_horizontal;
    private List<MyDataNovedades> data_list_horizontal;

    private RecyclerView recyclerView;
    private GridLayoutManager gridLayoutManager;
    private CustomAdapterNovedades adapter;
    private List<MyDataNovedades> data_list;
    View ChildView;
    int GetItemPosition ;

    private TextView menuprincipal, perfil, textvocacional, novedades;


    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novedades);

        //-----------------intent del menu------------------------------

        NavigationView navigationViewOfertas = (NavigationView) findViewById(R.id.nav_view_novedades);
        View view = navigationViewOfertas.getHeaderView(0);

        menuprincipal = (TextView) view.findViewById(R.id.novedades_home);
        menuprincipal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                home();
            }
        });

        perfil = (TextView)view.findViewById(R.id.peril_menu_principal_novedades);
        perfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ir_perfil();
            }
        });

        textvocacional = (TextView)view.findViewById(R.id.test_menu_principal_novedades);
        textvocacional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ir_test_vocacional();
            }
        });

        novedades = (TextView)view.findViewById(R.id.menu_principal_novedades);
        novedades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                novedades_ofertas();
            }
        });

        //----------Notificaciones-----------------------

        notificacion = new NotificationCompat.Builder(this);
        notificacion.setAutoCancel(true);


        FirebaseMessaging.getInstance().subscribeToTopic("news");
        String token = FirebaseInstanceId.getInstance().getToken();

        Log.d("token", token);
        registerToken(token);

        //----------sesion--------------------------------

        session = new SessionManager(getApplicationContext());
        if (!session.isLoggedIn()) {

        }



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


                //-----------servicios web horizontal ----------------------

                recyclerView_horizontal = (RecyclerView) findViewById(R.id.recycler_view_novedades_horizontal);
                data_list_horizontal  = new ArrayList<>();
                load_data_from_server_horizontal(0);

                adapter_horizontal = new CustomAdapterNhorizontal(this,data_list_horizontal);
                recyclerView_horizontal.setAdapter(adapter_horizontal);

                LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(Novedades.this, LinearLayoutManager.HORIZONTAL, false);
                recyclerView_horizontal.setLayoutManager(horizontalLayoutManagaer);


                //-----------servicios web ----------------------------------

                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Cargando Novedades...");
                progressDialog.show();

                recyclerView = (RecyclerView) findViewById(R.id.recycler_view_novedades);
                data_list  = new ArrayList<>();
                load_data_from_server(0);

                gridLayoutManager = new GridLayoutManager(this,1);
                recyclerView.setLayoutManager(gridLayoutManager);

                adapter = new CustomAdapterNovedades(this,data_list);
                recyclerView.setAdapter(adapter);

                recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {


                        if(gridLayoutManager.findLastCompletelyVisibleItemPosition() == data_list.size()){

                            load_data_from_server(data_list.get(data_list.size()).getId());
                            progressDialog.dismiss();
                        }

                        progressDialog.dismiss();

                    }
                });

                recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {

                    GestureDetector gestureDetector = new GestureDetector(Novedades.this, new GestureDetector.SimpleOnGestureListener() {

                        @Override public boolean onSingleTapUp(MotionEvent motionEvent) {

                            return true;
                        }

                    });
                    @Override
                    public boolean onInterceptTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {

                        ChildView = Recyclerview.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                        if(ChildView != null && gestureDetector.onTouchEvent(motionEvent)) {

                            GetItemPosition = Recyclerview.getChildAdapterPosition(ChildView);



                        }

                        return false;
                    }

                    @Override
                    public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

                    }

                    @Override
                    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                    }
                });



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_novedades);
        navigationView.setNavigationItemSelectedListener(this);

        //-----------------------------------------------

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.novedades, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_arrobamedellin) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://arrobamedellin.edu.co/"));
            startActivity(browserIntent);
        }else if (id == R.id.action_sapiencia) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.sapiencia.gov.co/"));
            startActivity(browserIntent);
        }else if (id == R.id.menu_ofertas_contactenos) {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
            emailIntent.setData(Uri.parse("mailto: app@arrobamedellin.edu.co"));
            startActivity(Intent.createChooser(emailIntent, "Enviar solicitud"));
        }else if(id == R.id.action_ayuda){
            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://campusdigital.arrobamedellin.edu.co/campus/recursos_arroba/documentos/terminos/ayuda.html"));
            startActivity(myIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.home) {

            home();

        } else if (id == R.id.novedades_ofertas) {

            if (session.isLoggedIn()){

                novedades_ofertas();
                finish();

            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(Novedades.this);
                builder.setTitle("No hay un usuario registrado");
                builder.setMessage("¿ Desea hacer el registro ?");
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Ir_test_vocacional();

                    }
                });
                builder.setNegativeButton(android.R.string.cancel, null);
                Dialog dialog = builder.create();
                dialog.show();
            }

        } else if (id == R.id.nav_perfil) {

            if (session.isLoggedIn()){

                Intent intent = new Intent(this, PerfilUsuario.class);
                startActivity(intent);
                finish();

            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(Novedades.this);
                builder.setTitle("No hay un usuario registrado");
                builder.setMessage("Desea hacer el registro?");
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Ir_test_vocacional();

                    }
                });
                builder.setNegativeButton(android.R.string.cancel, null);
                Dialog dialog = builder.create();
                dialog.show();
            }

        } else if (id == R.id.novedades_testvocacional) {

            Ir_test_vocacional();


        }else if (id == R.id.nav_share_sobre_nosotros) {

            Sobre_nosotros();

        } else if (id == R.id.nav_salir_novedades) {

            AlertDialog.Builder builder = new AlertDialog.Builder(Novedades.this);
            builder.setTitle("Salir de @Medellín");
            builder.setMessage("Desea salir de esta aplicacion?");
            builder.setPositiveButton("Salir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {


                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    finish();

                }
            });
            builder.setNegativeButton(android.R.string.cancel, null);
            Dialog dialog = builder.create();
            dialog.show();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void load_data_from_server_horizontal(final int id) {

        @SuppressLint("StaticFieldLeak") AsyncTask<Integer,Void,Void> task = new AsyncTask<Integer, Void, Void>() {

            @Override
            protected Void doInBackground(Integer... integers) {


                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://200.13.254.146/webserviceapp/scripthorizontal.php")
                        .build();


                try {
                    Response response = client.newCall(request).execute();


                    JSONArray array = new JSONArray(response.body().string());

                    for (int i=0; i<array.length(); i++){

                        JSONObject object = array.getJSONObject(i);

                        MyDataNovedades data = new MyDataNovedades(
                                object.getInt("id_novedad"),
                                object.getString("titulo"),
                                object.getString("contenido"),
                                object.getString("resumen"),
                                object.getString("created_at"),
                                object.getString("imagen"),
                                object.getString("link"),
                                object.getString("tipo_novedad"));

                        data_list_horizontal.add(data);
                    }



                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    System.out.println("End of content");
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                adapter_horizontal.notifyDataSetChanged();
            }
        };

        task.execute(id);
    }

    private void load_data_from_server(final int id) {

        @SuppressLint("StaticFieldLeak") AsyncTask<Integer,Void,Void> task = new AsyncTask<Integer, Void, Void>() {

            @Override
            protected Void doInBackground(Integer... integers) {


                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://200.13.254.146/webserviceapp/script.php")
                        .build();


                try {
                    Response response = client.newCall(request).execute();


                    JSONArray array = new JSONArray(response.body().string());

                    for (int i=0; i<array.length(); i++){

                        JSONObject object = array.getJSONObject(i);

                        MyDataNovedades data = new MyDataNovedades(
                                object.getInt("id_novedad"),
                                object.getString("titulo"),
                                object.getString("contenido"),
                                object.getString("resumen"),
                                object.getString("created_at"),
                                object.getString("imagen"),
                                object.getString("link"),
                                object.getString("tipo_novedad"));

                        data_list.add(data);
                    }



                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    System.out.println("End of content");
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                adapter.notifyDataSetChanged();
            }
        };

        task.execute(id);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            home();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void novedades_ofertas(){
        Intent intent =new Intent(this, Ofertas.class);
        startActivity(intent);
        finish();
    }

    private void Ir_test_vocacional(){
        Intent intent =new Intent(Novedades.this, RegistroUsuario.class);
        startActivity(intent);
        finish();
    }

    private void home(){
        Intent intent =new Intent(Novedades.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void Sobre_nosotros(){
        Intent intent =new Intent(this, SobreNosotros.class);
        startActivity(intent);
    }


    public static void registerToken(String token) {
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("token", token)
                .build();

        Request request = new Request.Builder()
                .url("http://200.13.254.146/webserviceapp/firebase/register_token.php?XDEBUG_SESSION_START=netbeans")
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("OkHttp", call.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String responseStr = response.body().string();
                    Log.d("OkHttp", responseStr);
                }
            }
        });

    }


    private void Ir_perfil(){

        if (session.isLoggedIn()){

            Intent intent = new Intent(this, PerfilUsuario.class);
            startActivity(intent);
            finish();

        }else {
            AlertDialog.Builder builder = new AlertDialog.Builder(Novedades.this);
            builder.setTitle("No hay un usuario registrado");
            builder.setMessage("Desea hacer el registro?");
            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Ir_test_vocacional();

                }
            });
            builder.setNegativeButton(android.R.string.cancel, null);
            Dialog dialog = builder.create();
            dialog.show();
        }
    }



}
