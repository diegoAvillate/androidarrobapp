package co.edu.arrobamedellin.arrobamedellin;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.edu.arrobamedellin.arrobamedellin.AdapterFiltros.AdapterAreaConocimiento;
import co.edu.arrobamedellin.arrobamedellin.DatosFiltros.DatosContentUniversidades;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ResultAvanzado extends AppCompatActivity {

    private RecyclerView recyclerView;
    private GridLayoutManager gridLayoutManager;
    private AdapterAreaConocimiento adapter;
    private List<DatosContentUniversidades> data_list;

    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_avanzado);



        ///-----------------------toolbar---------------------------

        toolbar = (Toolbar) findViewById(R.id.toolbar_menu_ofertas_resultados);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enter_Busqueda();
            }
        });

        ///----------------------recycler view------------------------------

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando ofertas...");
        progressDialog.show();

        recyclerView = (RecyclerView) findViewById(R.id.resultados_busqueda_avanzada);
        data_list = new ArrayList<>();
        load_data_from_server(0,getIntent().getExtras().getString("url"));

        gridLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(gridLayoutManager);

        adapter = new AdapterAreaConocimiento(this, data_list);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                progressDialog.dismiss();

            }
        });



    }

    private void load_data_from_server(final int id, final String url) {

        @SuppressLint("StaticFieldLeak") AsyncTask<Integer,Void,Void> task = new AsyncTask<Integer, Void, Void>() {

            @Override
            protected Void doInBackground(Integer... integers) {


                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(url)
                        .build();


                try {
                    okhttp3.Response response = client.newCall(request).execute();


                    JSONArray array = new JSONArray(response.body().string());

                    for (int i=0; i<array.length(); i++){

                        JSONObject object = array.getJSONObject(i);

                        DatosContentUniversidades data_ofertas = new DatosContentUniversidades(
                                object.getInt("id_ies_medellin"),
                                object.getString("codigo_institucion"),
                                object.getString("nombre_institucion"),
                                object.getString("estado_institucion"),
                                object.getString("caracter_academico"),
                                object.getString("sector"),
                                object.getString("codigo_snies"),
                                object.getString("area_de_conococimiento"),
                                object.getString("basico_de_conocimiento"),
                                object.getString("nombre_del_programa"),
                                object.getString("nivel_academico"),
                                object.getString("nivel_formacion"),
                                object.getString("metodologia"),
                                object.getString("numero_periodos_de_duracion"),
                                object.getString("periodos_de_duracion"),
                                object.getString("titulo_otorgado"),
                                object.getString("departamento_oferta_programa"),
                                object.getString("municipio_oferta_programa"),
                                object.getString("costo_matricula"),
                                object.getString("tiempo_admisiones_estudiantes"),
                                object.getString("Direccion google"),
                                object.getString("Logo universidad"));

                        data_list.add(data_ofertas);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    System.out.println("End of content");
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                adapter.notifyDataSetChanged();
            }
        };

        task.execute(id);
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            Enter_Busqueda();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void Enter_Busqueda() {
        Intent intent = new Intent(this, BusquedaAvanzada.class);
        startActivity(intent);
        finish();
    }



}
