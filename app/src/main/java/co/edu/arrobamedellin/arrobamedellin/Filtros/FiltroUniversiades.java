package co.edu.arrobamedellin.arrobamedellin.Filtros;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.edu.arrobamedellin.arrobamedellin.CustomAdapterOfertas;
import co.edu.arrobamedellin.arrobamedellin.DatosFiltros.DatosContentUniversidades;
import co.edu.arrobamedellin.arrobamedellin.MainActivity;
import co.edu.arrobamedellin.arrobamedellin.MyDataOfertas;
import co.edu.arrobamedellin.arrobamedellin.Ofertas;
import co.edu.arrobamedellin.arrobamedellin.R;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class FiltroUniversiades extends AppCompatActivity  implements SearchView.OnQueryTextListener{

    private RecyclerView recyclerView;
    private GridLayoutManager gridLayoutManager;
    private CustomAdapterOfertas adapter;
    private List<MyDataOfertas> data_list;
    View ChildView;
    int GetItemPosition ;
    ArrayList<String> Mydata;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtro_universiades);


        //------------action toolbal-------------------------

        toolbar = (Toolbar) findViewById(R.id.toolbar_buscar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enter_login();
            }
        });

        //-----------servicios web ----------------------------------

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando ofertas...");
        progressDialog.show();

        recyclerView = (RecyclerView) findViewById(R.id.ofertas_in);
        data_list  = new ArrayList<>();
        load_data_from_server(0);

        gridLayoutManager = new GridLayoutManager(this,1);
        recyclerView.setLayoutManager(gridLayoutManager);

        adapter = new CustomAdapterOfertas(this,data_list);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {


                if(gridLayoutManager.findLastCompletelyVisibleItemPosition() == data_list.size()){

                    load_data_from_server(data_list.get(data_list.size()).getId_universidad());
                    progressDialog.dismiss();
                }

                progressDialog.dismiss();

            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {

            GestureDetector gestureDetector = new GestureDetector(FiltroUniversiades.this, new GestureDetector.SimpleOnGestureListener() {

                @Override public boolean onSingleTapUp(MotionEvent motionEvent) {

                    return true;
                }

            });
            @Override
            public boolean onInterceptTouchEvent(RecyclerView Recyclerview, MotionEvent motionEvent) {

                ChildView = Recyclerview.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                if(ChildView != null && gestureDetector.onTouchEvent(motionEvent)) {

                    GetItemPosition = Recyclerview.getChildAdapterPosition(ChildView);



                }

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    private void load_data_from_server(final int id) {

        @SuppressLint("StaticFieldLeak") AsyncTask<Integer,Void,Void> task = new AsyncTask<Integer, Void, Void>() {

            @Override
            protected Void doInBackground(Integer... integers) {


                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://200.13.254.146/webserviceapp/script_ofertas.php")
                        .build();


                try {
                    Response response = client.newCall(request).execute();


                    JSONArray array = new JSONArray(response.body().string());

                    for (int i=0; i<array.length(); i++){

                        JSONObject object = array.getJSONObject(i);

                        MyDataOfertas data_ofertas = new MyDataOfertas(
                                object.getInt("id_universidad"),
                                object.getString("Código Institución"),
                                object.getString("Nombre Institución"),
                                object.getString("Naturaleza Jurídica"),
                                object.getString("Sector"),
                                object.getString("Carácter Académico"),
                                object.getString("Departamento Domicilio"),
                                object.getString("Municipio Domicilio"),
                                object.getString("Dirección Domicilio"),
                                object.getString("Teléfono Domicilio"),
                                object.getString("Estado"),
                                object.getString("Principal/Seccional"),
                                object.getString("Página Web"),
                                object.getString("¿Acreditada Alta Calidad?"),
                                object.getString("Fecha Acreditación"),
                                object.getString("Resolución de la acreditación"),
                                object.getString("Vigencia de la acreditación"),
                                object.getString("logo_universidad"));

                        data_list.add(data_ofertas);
                    }



                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    System.out.println("End of content");
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                adapter.notifyDataSetChanged();
            }
        };

        task.execute(id);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            Enter_login();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void Enter_login(){
        Intent intent =new Intent(this, Ofertas.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_filtros,menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        newText = newText.toLowerCase();
        ArrayList<MyDataOfertas> newList = new ArrayList<>();


        for (MyDataOfertas datosContentUniversidades : data_list){
            String name = datosContentUniversidades.getNombre_institucion().toLowerCase();
            if(name.contains(newText)){
                newList.add(datosContentUniversidades);
            }

        }
        adapter.setFilter(newList);


        return true;
    }
}
