package co.edu.arrobamedellin.arrobamedellin;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;

import org.angmarch.views.NiceSpinner;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import co.edu.arrobamedellin.arrobamedellin.AdapterFiltros.AdapterAreaConocimiento;
import co.edu.arrobamedellin.arrobamedellin.DatosFiltros.DatosContentUniversidades;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class BusquedaAvanzada extends AppCompatActivity {private RecyclerView recyclerView;

    private GridLayoutManager gridLayoutManager;
    private AdapterAreaConocimiento adapter;
    private List<DatosContentUniversidades> data_list;
    private String areas;
    private String universidad;
    private String resultado;
    List<String> areadeconocimientobusqueda;
    List<String>  result;
    LinkedList linkedList;
    Button tas1;
    private NiceSpinner areasConocimiento;
    private NiceSpinner sector;

    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busqueda_avanzada);

        toolbar = (Toolbar) findViewById(R.id.toolbar_menu_ofertas);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enter_Ofertas();
                finish();
            }
        });

        //----------------metodos de los spinner--------------------

        final NiceSpinner areadeconocimiento = (NiceSpinner) findViewById(R.id.spiner_area_de_conocimiento);
        areadeconocimiento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String url = "http://200.13.254.146/webserviceapp/script_busqueda_area_universidades.php?valor="+ String.valueOf(parent.getItemAtPosition(position-1));

                load_data_from_server_area(5,url);
                Log.d("RESULTADO",String.valueOf(url));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });





        Button tas1 = findViewById(R.id.tas1_buscar);
        tas1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NiceSpinner a = (NiceSpinner) findViewById(R.id.spiner_area_de_conocimiento);
                NiceSpinner s = (NiceSpinner) findViewById(R.id.spiner_tipo_institucion);
                NiceSpinner u = (NiceSpinner) findViewById(R.id.spiner_universidades);


                String url = "http://200.13.254.146/webserviceapp/script_busqueda_resultados.php?area="
                        + String.valueOf(a.getText())
                        + "&tipo=" + String.valueOf(s.getText())
                        + "&u="+ String.valueOf(u.getText());



                Intent i = new Intent(getApplicationContext(), ResultAvanzado.class);
                i.putExtra("url",url);
                startActivity(i);
                finish();

            }
        });


        //-----------------------------------------------------------
        areadeconocimientobusqueda=new ArrayList<>();
        load_data_from_server_AREAC(1);
        load_data_from_server_UNIVERSIDAD(0);





        final NiceSpinner sector = (NiceSpinner) findViewById(R.id.spiner_tipo_institucion);
        List<String> dataset = new LinkedList<>(Arrays.asList("SELECCIONE","PUBLICA", "PRIVADA"));
        sector.attachDataSource(dataset);
        sector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                NiceSpinner a = (NiceSpinner) findViewById(R.id.spiner_area_de_conocimiento);
                String s;
                if(position == 0) {
                    s = "SELECCIONE";
                } else if(position == 1) {
                    s = "PUBLICA";
                }else {
                    s = "PRIVADA";
                }

                String url = "http://200.13.254.146/webserviceapp/script_busqueda_tipo_universidad.php?area="+ String.valueOf(a.getText()) + "&sector=" + String.valueOf(s);

                load_data_from_server_area(2,url);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void load_data_from_server_AREAC(final int id) {

        @SuppressLint("StaticFieldLeak") AsyncTask<Integer,Void,Void> task = new AsyncTask<Integer, Void, Void>() {

            @Override
            protected Void doInBackground(Integer... integers) {
                try {
                Log.d("WEBSERVICE", "hola");


                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://200.13.254.146/webserviceapp/script_busqueda_area_conocimiento.php")
                        .build();

                areas  = "";



                    okhttp3.Response response = client.newCall(request).execute();


                    JSONArray array = new JSONArray(response.body().string());


                    for (int i=0; i<array.length(); i++){
                        areas = areas + array.getJSONObject(i).get("area_de_conococimiento").toString() + "#";

                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    System.out.println("End of content");
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                List<String> dataset = new LinkedList<>(Arrays.asList(areas.split("#")));
                NiceSpinner niceSpinner = (NiceSpinner) findViewById(R.id.spiner_area_de_conocimiento);
                niceSpinner.attachDataSource(dataset);

            }
        };

        task.execute(id);
    }




    private void load_data_from_server_UNIVERSIDAD(final int id) {

        @SuppressLint("StaticFieldLeak") AsyncTask<Integer,Void,Void> task = new AsyncTask<Integer, Void, Void>() {

            @Override
            protected Void doInBackground(Integer... integers) {


                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://200.13.254.146/webserviceapp/script_busqueda_universidades.php")
                        .build();

                universidad  = "";



                try {
                    okhttp3.Response response = client.newCall(request).execute();


                    JSONArray array = new JSONArray(response.body().string());



                    for (int i=0; i<array.length(); i++){


                        universidad = universidad + array.getJSONObject(i).get("nombre_institucion").toString() + ",";

                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    System.out.println("End of content");
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                List<String> dataset = new LinkedList<>(Arrays.asList(universidad.split(",")));
                NiceSpinner niceSpinner = (NiceSpinner) findViewById(R.id.spiner_universidades);
                niceSpinner.attachDataSource(dataset);

            }
        };

        task.execute(id);
    }



    private void load_data_from_server_area(final int id, final String UrlComplete) {

        @SuppressLint("StaticFieldLeak") AsyncTask<Integer,Void,Void> task = new AsyncTask<Integer, Void, Void>() {

            @Override
            protected Void doInBackground(Integer... integers) {


                try {

                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url(UrlComplete)
                            .build();

                    resultado  = "";


                    okhttp3.Response response = client.newCall(request).execute();

                    JSONArray array = new JSONArray(response.body().string());

                    if (array.length() > 0) {
                        for (int i = 0; i < array.length(); i++) {
                            resultado = resultado +array.getJSONObject(i).get("nombre_institucion").toString() + "#";
                        }
                    }

                    Log.d("RESULTADO",String.valueOf(array.length()));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    System.out.println("End of content");
                }

                return null;
            }

            protected void onPostExecute(Void aVoid) {

                List<String> dataset = new LinkedList<>(Arrays.asList(resultado.split("#")));
                NiceSpinner niceSpinner = (NiceSpinner) findViewById(R.id.spiner_universidades);
                niceSpinner.clearComposingText();
                niceSpinner.attachDataSource(dataset);
            }
        };

        task.execute(id);
    }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            Enter_Ofertas();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void Enter_Ofertas() {
        Intent intent = new Intent(this, Ofertas.class);
        startActivity(intent);
        finish();
    }
}
