package co.edu.arrobamedellin.arrobamedellin;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.getbase.floatingactionbutton.FloatingActionButton;

public class DetallesNovedades extends AppCompatActivity {

    Button menu_link;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_novedades);

        toolbar = (Toolbar) findViewById(R.id.toolbar_menu_novedades);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enter_Novedades();
            }
        });

        agregarFAB();
        agregarCompartir();

        //getSupportActionBar();


        String titulo = getIntent().getExtras().getString("titulo");
        String contenido = getIntent().getExtras().getString("contenido");
        String resumen = getIntent().getExtras().getString("resumen");
        String fecha = getIntent().getExtras().getString("fecha");
        String imagen_url = getIntent().getExtras().getString("imagen");

        //iniciar vista


        //CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.collaps_detalles_novedades);
        //collapsingToolbarLayout.setTitleEnabled(true);


        TextView tv_titulo = findViewById(R.id.aa_txt_titulo);
        TextView tv_contenido = findViewById(R.id.aa_txt_contenido);
        //TextView tv_resumen = findViewById(R.id.aa_txt_resumen);
        TextView tv_fecha = findViewById(R.id.aa_txt_fecha);
        ImageView tv_imagen = findViewById(R.id.aa_imageView_1);

        //configurando valores

        tv_titulo.setText(titulo);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            tv_contenido.setText(Html.fromHtml(contenido,Html.FROM_HTML_OPTION_USE_CSS_COLORS));
        }else{
            tv_contenido.setText(Html.fromHtml(contenido));
        }
        //tv_resumen.setText(resumen);
        tv_fecha.setText(fecha);

        //insertar imagen en glide

        Glide.with(this).load(imagen_url).into(tv_imagen);



    }

    public void agregarFAB(){

        final String link = getIntent().getExtras().getString("link");
        //Inicializamos el FAB
        FloatingActionButton miFAB = (FloatingActionButton) findViewById(R.id.accion_link);
        //Creamos el evento setOnClickListener para que se ejecute una accion al pulsar el FAB.
        miFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse(link);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
    }

    public void agregarCompartir(){

        final String link = getIntent().getExtras().getString("link");
        //Inicializamos el FAB
        FloatingActionButton miFAB = (FloatingActionButton) findViewById(R.id.accion_compartir);
        //Creamos el evento setOnClickListener para que se ejecute una accion al pulsar el FAB.
        miFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, link);
                startActivity(Intent.createChooser(intent, "Share with"));
            }
        });
    }

    private void Enter_Novedades() {
        Intent intent = new Intent(this, Novedades.class);
        startActivity(intent);
        finish();
    }
}
