package co.edu.arrobamedellin.arrobamedellin.AdapterFiltros;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.edu.arrobamedellin.arrobamedellin.DatosFiltros.DatosContentUniversidades;
import co.edu.arrobamedellin.arrobamedellin.Detallescarreras.DetallescarreraUnacional;
import co.edu.arrobamedellin.arrobamedellin.R;

public class AdapterSpinnerA extends RecyclerView.Adapter<AdapterSpinnerA.ViewHolder> {

    private Context context;
    private List<DatosContentUniversidades> my_data_ofertas;



    public AdapterSpinnerA(Context context, List<DatosContentUniversidades> my_data_ofertas) {
        this.context = context;
        this.my_data_ofertas = my_data_ofertas;
    }

    @Override
    public AdapterSpinnerA.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        view = layoutInflater.inflate(R.layout.card_area_conocimiento,parent,false);
        final AdapterSpinnerA.ViewHolder viewHolder = new AdapterSpinnerA.ViewHolder(view);
        return  viewHolder;
    }

    @Override
    public void onBindViewHolder(AdapterSpinnerA.ViewHolder holder, int position) {

        holder.area_conocimiento.setText("Área: " + my_data_ofertas.get(position).getArea_de_conococimiento());
        holder.nombre_programa.setText("Programa: " +my_data_ofertas.get(position).getNombre_del_programa());
        //holder.nombre_univercidad.setText(my_data_ofertas.get(position).getNombre_institucion());
        //holder.sector.setText(my_data_ofertas.get(position).getSector());



    }

    @Override
    public int getItemCount() {
        return my_data_ofertas.size();
    }



    public  class ViewHolder extends  RecyclerView.ViewHolder{

        public TextView area_conocimiento;
        public TextView nombre_univercidad;
        public TextView nombre_programa;
        public TextView sector;
        LinearLayout view_container;


        public ViewHolder(View itemView) {
            super(itemView);

            area_conocimiento = (TextView) itemView.findViewById(R.id.txt_area_conocimiento);
            nombre_univercidad= (TextView) itemView.findViewById(R.id.txt_nombre_universidad);
            nombre_programa = (TextView) itemView.findViewById(R.id.txt_nombre_programa);
            sector =(TextView) itemView.findViewById(R.id.txt_sector);


            view_container = itemView.findViewById(R.id.container_ofertas);
        }
    }

    public  void setFilter(ArrayList<DatosContentUniversidades> newlist){

        my_data_ofertas = new ArrayList<>();
        my_data_ofertas.addAll(newlist);
        notifyDataSetChanged();

    }
}
