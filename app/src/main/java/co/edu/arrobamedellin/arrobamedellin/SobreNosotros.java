package co.edu.arrobamedellin.arrobamedellin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;

public class SobreNosotros extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sobre_nosotros);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            Enter_login();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void Enter_login(){
        Intent intent =new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
