package co.edu.arrobamedellin.arrobamedellin;

public class MyDataNovedades {

    private int id_novedad;
    private String titulo;
    private String contenido;
    private String resumen;
    private String fecha;
    private String imagen;
    private String link;
    private String tipo_novedad;


    public MyDataNovedades(int id, String titulo, String contenido, String resumen, String fecha, String imagen, String link, String tipo_novedad) {
        this.id_novedad = id;
        this.titulo = titulo;
        this.contenido = contenido;
        this.resumen = resumen;
        this.fecha = fecha;
        this.imagen = imagen;
        this.link = link;
        this.tipo_novedad = tipo_novedad;
    }

    public int getId() {
        return id_novedad;
    }

    public void setId(int id) {
        this.id_novedad = id;
    }

    //----------------------------------------------

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    //--------------------------------------------------

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    //------------------------------------------------------

    public String getResumen() {
        return resumen;
    }
    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    //-----------------------------------------------------

    public String getFecha() {
        return fecha;
    }
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    //-----------------------------------------------------

    public String getImagen() {
        return imagen;
    }
    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getLink() {
        return link;
    }
    public void setLink(String link) {
        this.link = link;
    }

    public String getTipo_novedad() {
        return tipo_novedad;
    }

    public void setTipo_novedad(String tipo_novedad) {
        this.tipo_novedad = tipo_novedad;
    }
}
