package co.edu.arrobamedellin.arrobamedellin.Filtros;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.edu.arrobamedellin.arrobamedellin.AdapterFiltros.AdapterAreaConocimiento;
import co.edu.arrobamedellin.arrobamedellin.AdapterFiltros.AdapterPregrados;
import co.edu.arrobamedellin.arrobamedellin.AdapterFiltros.AdapterTecnologias;
import co.edu.arrobamedellin.arrobamedellin.DatosFiltros.DatosContentUniversidades;
import co.edu.arrobamedellin.arrobamedellin.Ofertas;
import co.edu.arrobamedellin.arrobamedellin.R;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class FiltroPregrados extends AppCompatActivity implements SearchView.OnQueryTextListener{

    private RecyclerView recyclerView;
    private GridLayoutManager gridLayoutManager;
    //private AdapterPregrados adapter;
    private AdapterAreaConocimiento adapter;
    private List<DatosContentUniversidades> data_list;
    View ChildView;
    int GetItemPosition ;
    ArrayList<String> Mydata;

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtro_pregrados);

        toolbar = (Toolbar) findViewById(R.id.toolbar_buscar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enter_Ofertas();
            }
        });


        //-----------servicios web ----------------------------------

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando ofertas...");
        progressDialog.show();

        recyclerView = (RecyclerView) findViewById(R.id.ofertas_in_filtros_pregrados);
        data_list = new ArrayList<>();
        load_data_from_server(0);

        gridLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(gridLayoutManager);

        //adapter = new AdapterPregrados(this, data_list);
        adapter = new AdapterAreaConocimiento(this, data_list);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {


                if (gridLayoutManager.findLastCompletelyVisibleItemPosition() == data_list.size()) {

                    load_data_from_server(data_list.get(data_list.size()).getId_ies_medellin());
                    progressDialog.dismiss();
                }

                progressDialog.dismiss();

            }
        });

    }

    private void load_data_from_server(final int id) {

        @SuppressLint("StaticFieldLeak") AsyncTask<Integer,Void,Void> task = new AsyncTask<Integer, Void, Void>() {

            @Override
            protected Void doInBackground(Integer... integers) {


                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://200.13.254.146/webserviceapp/scrip_pregrado.php")
                        .build();


                try {
                    okhttp3.Response response = client.newCall(request).execute();


                    JSONArray array = new JSONArray(response.body().string());

                    for (int i=0; i<array.length(); i++){

                        JSONObject object = array.getJSONObject(i);

                        DatosContentUniversidades data_ofertas = new DatosContentUniversidades(
                                object.getInt("id_ies_medellin"),
                                object.getString("codigo_institucion"),
                                object.getString("nombre_institucion"),
                                object.getString("estado_institucion"),
                                object.getString("caracter_academico"),
                                object.getString("sector"),
                                object.getString("codigo_snies"),
                                object.getString("area_de_conococimiento"),
                                object.getString("basico_de_conocimiento"),
                                object.getString("nombre_del_programa"),
                                object.getString("nivel_academico"),
                                object.getString("nivel_formacion"),
                                object.getString("metodologia"),
                                object.getString("numero_periodos_de_duracion"),
                                object.getString("periodos_de_duracion"),
                                object.getString("titulo_otorgado"),
                                object.getString("departamento_oferta_programa"),
                                object.getString("municipio_oferta_programa"),
                                object.getString("costo_matricula"),
                                object.getString("tiempo_admisiones_estudiantes"),
                                object.getString("Direccion google"),
                                object.getString("Logo universidad"));
                        data_list.add(data_ofertas);
                    }



                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    System.out.println("End of content");
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                adapter.notifyDataSetChanged();
            }
        };

        task.execute(id);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        newText = newText.toLowerCase();
        ArrayList<DatosContentUniversidades> newList = new ArrayList<>();

        for (DatosContentUniversidades datosContentUniversidades : data_list){
            String name = datosContentUniversidades.getNombre_del_programa().toLowerCase();
            if(name.contains(newText)){
                newList.add(datosContentUniversidades);
            }

        }
        adapter.setFilter(newList);


        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            Enter_Ofertas();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void Enter_Ofertas(){
        Intent intent =new Intent(this, Ofertas.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_filtros,menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setOnQueryTextListener(this);
        return true;
    }
}
