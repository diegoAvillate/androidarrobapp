package co.edu.arrobamedellin.arrobamedellin.AdapterFiltros;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import co.edu.arrobamedellin.arrobamedellin.DatosFiltros.DatosContentUniversidades;
import co.edu.arrobamedellin.arrobamedellin.DetallesNovedades;
import co.edu.arrobamedellin.arrobamedellin.Detallescarreras.DetallescarreraUnacional;
import co.edu.arrobamedellin.arrobamedellin.R;

public class AdapterContentUniversidades extends RecyclerView.Adapter<AdapterContentUniversidades.ViewHolder> {

    private Context context;
    private List<DatosContentUniversidades> my_data_ofertas;



    public AdapterContentUniversidades(Context context, List<DatosContentUniversidades> my_data_ofertas) {
        this.context = context;
        this.my_data_ofertas = my_data_ofertas;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        view = layoutInflater.inflate(R.layout.card_unacional,parent,false);
        final ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.view_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DetallescarreraUnacional.class);
                i.putExtra("nombre_institucion",my_data_ofertas.get(viewHolder.getAdapterPosition()).getNombre_institucion());
                i.putExtra("estado_institucion",my_data_ofertas.get(viewHolder.getAdapterPosition()).getEstado_institucion());
                i.putExtra("caracter_academico",my_data_ofertas.get(viewHolder.getAdapterPosition()).getCaracter_academico());
                i.putExtra("sector",my_data_ofertas.get(viewHolder.getAdapterPosition()).getSector());
                i.putExtra("codigo_snies",my_data_ofertas.get(viewHolder.getAdapterPosition()).getCodigo_snies());
                i.putExtra("area_de_conococimiento", my_data_ofertas.get(viewHolder.getAdapterPosition()).getArea_de_conococimiento());
                i.putExtra("basico_de_conocimiento", my_data_ofertas.get(viewHolder.getAdapterPosition()).getBasico_de_conocimiento());
                i.putExtra("nombre_del_programa", my_data_ofertas.get(viewHolder.getAdapterPosition()).getNombre_del_programa());
                i.putExtra("nivel_academico", my_data_ofertas.get(viewHolder.getAdapterPosition()).getNivel_academico());
                i.putExtra("nivel_formacion", my_data_ofertas.get(viewHolder.getAdapterPosition()).getNivel_formacion());
                i.putExtra("metodologia", my_data_ofertas.get(viewHolder.getAdapterPosition()).getMetodologia());
                i.putExtra("numero_periodos_de_duracion", my_data_ofertas.get(viewHolder.getAdapterPosition()).getNumero_periodos_de_duracion());
                i.putExtra("periodos_de_duracion", my_data_ofertas.get(viewHolder.getAdapterPosition()).getPeriodos_de_duracion());
                i.putExtra("titulo_otorgado", my_data_ofertas.get(viewHolder.getAdapterPosition()).getTitulo_otorgado());
                i.putExtra("departamento_oferta_programa", my_data_ofertas.get(viewHolder.getAdapterPosition()).getDepartamento_oferta_programa());
                i.putExtra("municipio_oferta_programa", my_data_ofertas.get(viewHolder.getAdapterPosition()).getMunicipio_oferta_programa());
                i.putExtra("costo_matricula", my_data_ofertas.get(viewHolder.getAdapterPosition()).getCosto_matricula());
                i.putExtra("tiempo_admisiones_estudiantes", my_data_ofertas.get(viewHolder.getAdapterPosition()).getTiempo_admisiones_estudiantes());
                i.putExtra("Direccion google",my_data_ofertas.get(viewHolder.getAdapterPosition()).getDireccion_google());
                i.putExtra("Logo universidad",my_data_ofertas.get(viewHolder.getAdapterPosition()).getLogo_universidad());
                context.startActivity(i);

            }
        });
        return  viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.area_conocimiento.setText("Área: " + my_data_ofertas.get(position).getArea_de_conococimiento());
        holder.nombre_programa.setText(my_data_ofertas.get(position).getNombre_del_programa());
        Glide.with(context).load(my_data_ofertas.get(position).getLogo_universidad()).into(holder.logo_u);
        holder.sector.setText("Sector: " + my_data_ofertas.get(position).getSector());


    }

    @Override
    public int getItemCount() {
        return my_data_ofertas.size();
    }



    public  class ViewHolder extends  RecyclerView.ViewHolder{

        public TextView area_conocimiento;
        public TextView nombre_universidad;
        public TextView nombre_programa;
        public ImageView logo_u;
        public TextView sector;
        LinearLayout view_container;


        public ViewHolder(View itemView) {
            super(itemView);

            area_conocimiento = (TextView) itemView.findViewById(R.id.txt_area_conocimiento);
            nombre_universidad= (TextView) itemView.findViewById(R.id.txt_nombre_universidad);
            nombre_programa = (TextView) itemView.findViewById(R.id.txt_nombre_programa);
            logo_u = (ImageView)itemView.findViewById(R.id.img_logo_u);
            sector =(TextView) itemView.findViewById(R.id.txt_sector);


            view_container = itemView.findViewById(R.id.container_ofertas);
        }
    }

    public  void setFilter(ArrayList<DatosContentUniversidades> newlist){

        my_data_ofertas = new ArrayList<>();
        my_data_ofertas.addAll(newlist);
        notifyDataSetChanged();

    }
}
