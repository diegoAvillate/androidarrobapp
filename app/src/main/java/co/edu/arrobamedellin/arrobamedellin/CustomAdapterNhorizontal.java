package co.edu.arrobamedellin.arrobamedellin;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Random;

public class CustomAdapterNhorizontal extends  RecyclerView.Adapter<CustomAdapterNhorizontal.ViewHolder>{

    private Context context;
    private List<MyDataNovedades> my_data;
    private final static int FADE_DURATION = 1000; //FADE_DURATION in milliseconds





    public CustomAdapterNhorizontal(Context context, List<MyDataNovedades> my_data) {
        this.context = context;
        this.my_data = my_data;
    }

    @Override
    public CustomAdapterNhorizontal.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view;
        LayoutInflater minflater = LayoutInflater.from(context);
        view = minflater.inflate(R.layout.card_novedades_horizontal,parent,false);

        final CustomAdapterNhorizontal.ViewHolder viewHolder = new CustomAdapterNhorizontal.ViewHolder(view);
        viewHolder.view_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DetallesNovedades.class);
                i.putExtra("titulo",my_data.get(viewHolder.getAdapterPosition()).getTitulo());
                i.putExtra("contenido",my_data.get(viewHolder.getAdapterPosition()).getContenido());
                i.putExtra("resumen",my_data.get(viewHolder.getAdapterPosition()).getResumen());
                i.putExtra("fecha",my_data.get(viewHolder.getAdapterPosition()).getFecha());
                i.putExtra("imagen",my_data.get(viewHolder.getAdapterPosition()).getImagen());
                i.putExtra("link", my_data.get(viewHolder.getAdapterPosition()).getLink());

                context.startActivity(i);
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomAdapterNhorizontal.ViewHolder holder, int position) {

        holder.titulo.setText(my_data.get(position).getTitulo());
        holder.resumen.setText(my_data.get(position).getResumen());
        holder.fecha.setText(my_data.get(position).getFecha());
        Glide.with(context).load(my_data.get(position).getImagen()).into(holder.imagen);
        setScaleAnimation(holder.itemView);
       // setAnimation(holder.itemView,position);

    }

    @Override
    public int getItemCount() {
        return my_data.size();
    }

    public  class ViewHolder extends  RecyclerView.ViewHolder{

        public TextView titulo;
        public TextView resumen;
        public TextView fecha;
        public ImageView imagen;
        LinearLayout view_container;


        public ViewHolder(View itemView) {
            super(itemView);

            titulo = (TextView) itemView.findViewById(R.id.txt_titulo);
            resumen= (TextView) itemView.findViewById(R.id.txt_resumen);
            fecha = (TextView) itemView.findViewById(R.id.txt_fecha);
            imagen = (ImageView) itemView.findViewById(R.id.imageView_1);

            view_container = itemView.findViewById(R.id.container_novedades);


        }
    }
    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(FADE_DURATION);
        view.startAnimation(anim);
    }
    private void setScaleAnimation(View view) {
        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(FADE_DURATION);
        view.startAnimation(anim);
    }

    private int lastPosition = -1;

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(new Random().nextInt(501));//to make duration random number between [0,501)
            viewToAnimate.startAnimation(anim);
            lastPosition = position;
        }
    }
}
