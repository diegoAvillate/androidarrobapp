package co.edu.arrobamedellin.arrobamedellin;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.edu.arrobamedellin.arrobamedellin.AdapterFiltros.AdapterAreaConocimiento;
import co.edu.arrobamedellin.arrobamedellin.DatosFiltros.DatosContentUniversidades;
import co.edu.arrobamedellin.arrobamedellin.Filtros.FiltroAConocimiento;
import co.edu.arrobamedellin.arrobamedellin.Filtros.FiltroPregrados;
import co.edu.arrobamedellin.arrobamedellin.Filtros.FiltroTecnologia;
import co.edu.arrobamedellin.arrobamedellin.Filtros.FiltroUniversiades;
import co.edu.arrobamedellin.arrobamedellin.Filtros.FiltrosPosgrados;
import co.edu.arrobamedellin.arrobamedellin.helper.SessionManager;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class Ofertas extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private SessionManager session;

    private TextView btn_busqueda_avanzada;
    private TextView txtcarrera, txtuniversidad, tipoinstitucion;
    private TextView menuprincipal, perfil, textvocacional, novedades;
    private TextView tecnologia, pregrado, posgrado, maestria, doctorado;



    //--------------menu------------------------------------




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ofertas);

        btn_busqueda_avanzada = (TextView)findViewById(R.id.btn_busqueda_avanzada);
        btn_busqueda_avanzada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ir_busqueda_avanzada();
            }
        });

        txtuniversidad = (TextView)findViewById(R.id.ofetas_universidad);
        txtuniversidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ir_universidades();
            }
        });

        txtcarrera = (TextView)findViewById(R.id.ofertas_carreras);
        txtcarrera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ir_area_conocimiento();
            }
        });

        //-----------------intent del menu------------------------------

        NavigationView navigationViewOfertas = (NavigationView) findViewById(R.id.nav_view_ofertas);
        View view = navigationViewOfertas.getHeaderView(0);

        menuprincipal = (TextView) view.findViewById(R.id.ofertas_home);
        menuprincipal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                home();
            }
        });

        perfil = (TextView)view.findViewById(R.id.perfil_menu_ofetas);
        perfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ir_perfil();
            }
        });

        textvocacional = (TextView)view.findViewById(R.id.test_menu_ofertas);
        textvocacional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ir_test_vocacional();
            }
        });

        novedades = (TextView)view.findViewById(R.id.novedades_menu_ofertas);
        novedades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ofetas_novedades();
            }
        });

        //------------------niveles de formacion-----------------------

        tecnologia = (TextView) view.findViewById(R.id.ofertas_tecnologia);
        tecnologia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ir_a_tecnologias();
            }
        });

        pregrado = (TextView) view.findViewById(R.id.ofertas_pregrado);
        pregrado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ir_a_pregrados();
            }
        });

        posgrado =(TextView) view.findViewById(R.id.ofertas_posgrados);
        posgrado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ir_a_posgrados();
            }
        });


        //----------------- verificar sesion-----------------------------


        session = new SessionManager(getApplicationContext());
        if (!session.isLoggedIn()) {

        }

        //------------------control toolbar------------------------------


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_ofertas);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.title_activity_ofertas);

        //------------------control menu drawer---------------------------



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_ofertas);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_ofertas);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_ofertas);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.ofertas, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_arrobamedellin) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://arrobamedellin.edu.co/"));
            startActivity(browserIntent);
        }else if (id == R.id.action_sapiencia) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.sapiencia.gov.co/"));
            startActivity(browserIntent);
        }else if (id == R.id.menu_ofertas_contactenos) {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
            emailIntent.setData(Uri.parse("mailto: app@arrobamedellin.edu.co"));
            startActivity(Intent.createChooser(emailIntent, "Enviar solicitud"));
        }else if(id == R.id.action_ayuda){
            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://campusdigital.arrobamedellin.edu.co/campus/recursos_arroba/documentos/terminos/ayuda.html"));
            startActivity(myIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.home) {

            home();

        }else if (id == R.id.mi_perfil) {
            Ir_perfil();

        }else  if (id == R.id.ofertas_novedades) {
            ofetas_novedades();

        }else if (id == R.id.filtro_Universidades) {
            Ir_universidades();

        }else if (id == R.id.filtro_Area_conocimiento) {

            Ir_area_conocimiento();

        }else if (id == R.id.ofertas_testvocacional) {

            Ir_test_vocacional();

        }else if (id == R.id.filtro_tecnologia) {
            Ir_a_tecnologias();

        }else if (id == R.id.filtro_pregrados) {
            Ir_a_pregrados();
        }
        else if (id == R.id.filtros_posgrados) {
            Ir_a_posgrados();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_ofertas);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            Enter_login();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void Enter_login(){
        Intent intent =new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
    private void Ir_perfil(){

        if (session.isLoggedIn()){

            Intent intent = new Intent(this, PerfilUsuario.class);
            startActivity(intent);
            finish();

        }else {
            AlertDialog.Builder builder = new AlertDialog.Builder(Ofertas.this);
            builder.setTitle("No hay un usuario registrado");
            builder.setMessage("Desea hacer el registro?");
            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Ir_test_vocacional();

                }
            });
            builder.setNegativeButton(android.R.string.cancel, null);
            Dialog dialog = builder.create();
            dialog.show();
        }
    }

    private void Ir_area_conocimiento(){
        Intent intent =new Intent(Ofertas.this, FiltroAConocimiento.class);
        startActivity(intent);
        finish();
    }
    private void Ir_a_tecnologias(){
        Intent intent =new Intent(Ofertas.this, FiltroTecnologia.class);
        startActivity(intent);
        finish();
    }
    private void Ir_a_pregrados(){
        Intent intent =new Intent(Ofertas.this, FiltroPregrados.class);
        startActivity(intent);
        finish();
    }
    private void Ir_a_posgrados(){
        Intent intent =new Intent(Ofertas.this, FiltrosPosgrados.class);
        startActivity(intent);
        finish();
    }

    private void Ir_universidades(){
        Intent intent =new Intent(Ofertas.this, FiltroUniversiades.class);
        startActivity(intent);
        finish();
    }

    private void ofetas_novedades(){
        Intent intent =new Intent(Ofertas.this, Novedades.class);
        startActivity(intent);
        finish();
    }

    private void Ir_test_vocacional(){
        Intent intent =new Intent(Ofertas.this, RegistroUsuario.class);
        startActivity(intent);
        finish();
    }

    private void home(){
        Intent intent =new Intent(Ofertas.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void Ir_busqueda_avanzada(){
        Intent intent =new Intent(Ofertas.this, BusquedaAvanzada.class);
        startActivity(intent);
        finish();
    }


}
